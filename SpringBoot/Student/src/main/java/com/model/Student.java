package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Student {
	
	@Id@GeneratedValue
	private int rollNo;
	
	@Column(name = "sname")
	private String studentName;
	private int age;
	private String gender;
	
	public Student() {
		super();
	}

	public Student(int rollNo, String studentName, int age, String gender) {
		this.rollNo = rollNo;
		this.studentName = studentName;
		this.age = age;
		this.gender = gender;
	}

	public int getRollNo() {
		return rollNo;
	}

	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	@Override
	public String toString() {
		return "Student [rollNo=" + rollNo + ", studentName=" + studentName + ", age=" + age + ", gender=" + gender
				+ "]";
	}

	
}