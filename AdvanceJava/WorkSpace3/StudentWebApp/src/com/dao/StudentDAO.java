package com.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;
import com.dto.Student;

public class StudentDAO {

	public Student stuLogin(String emailId, String password) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String loginQuery = "Select * from student where emailId = ? and password = ?";
		
		try {
			pst = con.prepareStatement(loginQuery);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				Student student = new Student();
				
				student.setrollNo(rs.getInt(1));
				student.setStudentName(rs.getString(2));
				
				student.setGender(rs.getString(3));
				student.setClasses(rs.getString(4));
				student.setEmailId(rs.getString(5));
				student.setPassword(rs.getString(6));
				
				return student;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
}



