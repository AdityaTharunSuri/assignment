package com.dto;

public class Student {
	private int rollNo;
	private String StudentName;
	private String gender;
	private String Class;
	private String emailId;
	private String password;
	
	public Student() {
		super();
	}

	public Student(int rollNo, String StudentName,  String gender,String Class, String emailId, String password) {
		super();
		this.rollNo = rollNo;
		this.StudentName = StudentName;
		this.gender = gender;
		this.Class=Class;
		this.emailId = emailId;
		this.password = password;
	}

	public int getrollNo() {
		return rollNo;
	}
	public void setrollNo(int rollNo) {
		this.rollNo = rollNo;
	}

	public String getStudentName() {
		return StudentName;
	}
	public void setStudentName(String StudentName) {
		this.StudentName = StudentName;
	}

	

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getClasses() {
		return Class;
	}
	public void setClasses(String Class) {
		this.Class = Class;
	}

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Student [rollNo=" + rollNo + ", StudentName=" + StudentName + ", Class=" + Class + ", gender=" + gender
				+ ", emailId=" + emailId + ", password=" + password + "]";
	}



}
